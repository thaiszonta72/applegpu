import xml.etree.ElementTree as ET
import sys
from mako.template import Template

xml = sys.argv[1]
instructions = ET.parse(xml).getroot().findall('instruction')

def parse_field(field):
    if field.get('start', None):
        return [(int(field.get('start')), int(field.get('size')), 0)]
    else:
        return [(int(r.get('start')), int(r.get('size')), int(r.get('shift'))) for r in field.findall('sub')]

def extract_bits(start, size):
    assert ((start < 64) == ((start + size) <= 64))
    f = "lo" if start < 64 else "hi"
    start &= 63

    return "((" + f + " >> " + str(start) + "ull)" + " & " + hex((1 << size) - 1) + ")"

def extract_field(field):
    parsed = parse_field(field)
    parts = ["(" + extract_bits(p[0], p[1]) + " >> " + str(p[2]) + ")" for p in parsed]
    return "unsigned {} = {};".format(field.get('name'), " | ".join(parts))

def print_operand(operand):
    name = operand.get('name')
    T = operand.get('type', None)

    if operand.tag == 'field':
        return ''
    elif operand.tag == 'dst':
        out = 'agx_print_dst(fp, {}, {});'.format(name, name + 't')
        if T == 'f':
            out += ' if (S) { fprintf(fp, ".sat"); }'
        return out
    elif operand.tag == 'src':
        out = 'agx_print_src(fp, {}, {}, {});'.format(name, name + 't', int(T == 'f'))
        if T == 'f':
            out += ' if ({}m & 1) {{ fprintf(fp, ".abs"); }}'.format(name)
            out += ' if ({}m & 2) {{ fprintf(fp, ".neg"); }}'.format(name)
        elif T == 'i':
            out += ' if ({}s & 1) {{ fprintf(fp, ".sx"); }}'.format(name)

        return out
    elif operand.tag == 'immediate':
        return 'fprintf(fp, "#0x%X", {});'.format(name, name)
    elif operand.tag == 'shift':
        return 'if ({}) fprintf(fp, " lsl %u", {});'.format(name, name)
    elif operand.tag == 'memory_reg':
        return 'if ({}t & 1) {{ agx_print_16(fp, \'r\', {}); }} else {{ fprintf(fp, "r%u", {} >> 1); }}'.format(name, name, name)
    elif operand.tag == 'memory_index':
        return 'if ({}t) {{ int16_t sext = {}; fprintf(fp, "#%d", sext); }} else {{ assert(({} & 1) == 0 && {} < 0x100); fprintf(fp, "r%u", {} >> 1); }}'.format(name, name, name, name, name)
    elif operand.tag == 'mask':
        return 'fprintf(fp, "mask:%X", {});'.format(name)
    elif operand.tag == 'format':
        return 'fprintf(fp, "format:%X", {});'.format(name)
    else:
        return "/* TODO */"

# Generate matcher
TEMPLATE = """#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <inttypes.h>
#include <math.h>

static void
agx_print_16(FILE *fp, char prefix, unsigned value)
{
    fprintf(fp, "%c%u%c", prefix, value >> 1, (value & 1) ? 'h' : 'l');
}

static float
decode_float_immediate(uint8_t n)
{
    float sign = (n & 0x80) ? -1.0 : 1.0;
	unsigned e = (n & 0x70) >> 4;
	unsigned f_i = n & 0xF;
	float f = f_i;
    if (e == 0) {
		return sign * f / 64.0f;
    } else {
		return ldexp(sign * (float) (0x10 | f_i), e - 7);
    }
}

static void
agx_print_src(FILE *fp, unsigned value, unsigned flags, bool is_float)
{
    if (flags == 0) {
        if (is_float)
            fprintf(fp, "#%f", decode_float_immediate(value));
        else
            fprintf(fp, "#%u", value);
    } else if ((flags & 0xC) == 0x4) {
        value |= (flags & 1) << 8;
        if (flags & 2) {
            assert((value & 1) == 0);
            fprintf(fp, "u%u", value >> 1);
        } else {
            agx_print_16(fp, 'u', value);
        }
    } else if ((flags & 3) != 0) {
        if (flags & 0x2)
            fprintf(fp, "%c", (flags & 0x1) ? '`' : '$');

        if ((flags & 0xC) == 0xC) {
            fprintf(fp, "r%u:r%u", (value >> 1), (value >> 1) + 1);
        } else if (flags & 0x8) {
            fprintf(fp, "r%u", (value >> 1));
        } else {
            agx_print_16(fp, 'r', value);
        }
    } else {
        fprintf(fp, "TODO");
    }
}

static void
agx_print_dst(FILE *fp, unsigned value, unsigned flags)
{
    if (flags & 1)
        fprintf(fp, "$");

    /* TODO: check bit sizes */
    if (flags & 0x2) {
        if (value & 1)
            fprintf(fp, "r%u:r%u", value >> 1, (value >> 1) + 1);
        else
            fprintf(fp, "r%u", value >> 1);
    } else {
        fprintf(fp, "r%u%c", value >> 1, (value & 0x1) ? 'h' : 'l');
    }
}

unsigned
agx_disasm_instr(FILE *fp, uint8_t *code, bool *stop)
{
    uint64_t lo, hi;
    memcpy(&lo, code, 8);
    memcpy(&hi, code + 8, 8);

% for i, ins in enumerate(instructions):
<%
    mask = int(ins.get('mask'), 16)
    bits = int(ins.get('bits'), 16)
    assert((mask >> 64) == 0)

    # Determine size
    size_1 = 1
    size_2 = 2
    length_bit = None
    if ins.get('size_1', None):
        size_1 = int(ins.get('size_1'))
        size_2 = int(ins.get('size_2'))
        length_bit = int(ins.get('length_bit_pos'))
        assert(length_bit < 64)
    else:
        size_1 = int(ins.get('size'))
        size_2 = size_1

    prefix = "else " if i > 0 else ""
%>
    ${prefix}if ((lo & ${hex(mask)}ull) == ${hex(bits)}ull) {
% if length_bit:
        unsigned size = (lo & (1ull << ${length_bit})) ? ${size_2} : ${size_1};
% else:
        unsigned size = ${size_1};
% endif
        if (size > 8) {
            hi &= ((1ull << (((uint64_t) (size - 8)) * 8ull)) - 1);
        } else {
            hi = 0;
            lo &= ((1ull << (((uint64_t) (size)) * 8ull)) - 1);
        }

% if ins.get('name') == 'stop':
    *stop = true;
% endif
        fprintf(fp, "${ins.get('name')} ");

% for field in ins.findall('field'):
        ${extract_field(field)}
% endfor
% for i, operand in enumerate([x for x in ins.findall('*') if x.tag != 'field']):
% if i > 0:
    fprintf(fp, ", ");
% endif
    ${print_operand(operand)}
% endfor
        fprintf(fp, "\\n");
        return size;

    }
% endfor
    else {
        fprintf(fp, "** DISASSEMBLY FAILED ** %08"PRIx64" %08" PRIx64 "\\n", lo, hi);
        return 2;
    }
}
"""

print(Template(TEMPLATE).render(instructions = instructions, extract_field = extract_field, print_operand = print_operand))
